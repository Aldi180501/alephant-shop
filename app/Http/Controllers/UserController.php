<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    //
    public function register(Request $request)
    {
        User::create($request->all());
        return redirect()->route('login.page')->with(['success' => 'Registrasi Berhasil']);
    }

    public function login(Request $request)
    {
        $user = User::where('email_user', $request->email_user)->first();
        if ($user->password_user == $request->password_user) {
            return 'Berhasil';
        }
    }
}
