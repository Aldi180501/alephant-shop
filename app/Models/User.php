<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;

    protected $fillable = [
        'name_user',
        'email_user',
        'password_user',
        'alamat_user',
        'no_telp_user',
    ];
}
